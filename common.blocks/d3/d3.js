modules.define('d3',['loader_type_js','d3__config'],function(provide, loader, cfg) {

loader(cfg.url, function() {
    provide(d3);
});

});

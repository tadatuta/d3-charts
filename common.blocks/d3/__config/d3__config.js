/**
 * @module d3__config
 * @description Configuration for d3
 */

modules.define('d3__config', function(provide) {

provide(/** @exports */{
    /**
     * URL for loading d3 if it does not exist
     * @type {String}
     */
    url : 'https://d3js.org/d3.v3.min.js'
});

});

block('chart')(
	js()(true),
	content()(function(){
		var ctx = this.ctx;
		//mods
		var selecteBlock = { block: 'liner-chart'};
		return [
			{ elem: 'title', content: ctx.title },
			{ elem: 'graphic', content: selecteBlock },
			{ elem: 'legend', content: 'chart legend' }
		];
	}),
	elem('title')(
		tag()('h4')
	),
	elem('graphic')(
	),
	elem('legend')(		
	)
);